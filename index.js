import fsPromise from 'fs/promises';
import path from 'path';

const newspostSchema = {
  id: Number,
  title: String,
  text: String,
  createDate: Date,
};

class FileDB {

    async registerSchema(tableName, schema) {
        try {
            const filePath = `${tableName}.json`;
            const data = {
                table: [],
                id: 1,
                schema: schema
            };
            await fsPromise.writeFile(filePath, JSON.stringify(data));
            console.log(`File ${filePath} created`);
        } catch (err) {
            console.error(err);
            throw err;
        }
    }
    

    getTable(tableName) {
        return new TableOps(tableName);
    }
}

class Post {
    constructor(id, title, text) {
        this.id = id;
        this.title = title;
        this.text = text;
        this.createDate = new Date().toISOString();
    }
}

class TableOps {

    constructor(tableName) {
        this.tableName = tableName;
        this.filePath = `${this.tableName}.json`;
    }

    async readTable() {
        try {
            const data = await fsPromise.readFile(this.filePath, 'utf-8');
            // console.log(data);
            // console.log(items);
            return JSON.parse(data);

        } catch (err) {
            console.error(err);
            throw err;
        }
    }

    async create(data) {
        try {
            const { text, title } = data;
            let { id, schema, table } = await this.readTable();

            
            const newPost = new Post({ id, title, text });
            const newId = id + 1;

            for (const key in schema) {
                if (!(key in newPost)) {
                    throw new Error(`Missing property '${key}' in the provided data`);
                }
            }     

            table.push(newPost);

            await fsPromise.writeFile(this.filePath, JSON.stringify({ table, id: newId, schema } ));
            return newPost;

        } catch (err) {
            console.error(err);
            throw err;
        }       
    }

    async update(id, data) {
        try {

            const { text, title } = data;
            let { schema, table } = await this.readTable();
            console.log(table);
            // Знайти індекс запису з вказаним ідентифікатором
            const index = table.findIndex(post => post.id === id);

            // Якщо запис не знайдено, викинути помилку
            if (index === -1) {
                throw new Error(`Record with id ${id} not found`);
            }

            // Оновити дані запису з новими даними
            const updatedPost = { ...table[index], ...data };
            table[index] = updatedPost;

            // Зберегти оновлені дані у файл
            await fsPromise.writeFile(this.filePath, JSON.stringify({ table, schema }));
            return updatedPost;

        } catch (err) {
            console.error(err);
            throw err;
        }
    }

    async getAll() {
        try {
            const { table } = await this.readTable();
            return table;
        } catch (err) {
            console.error(err);
            throw err;
        }
    }

    async getById(id) {
        try {
            const { table } = await this.readTable();

            // Знайти запис за заданим ідентифікатором
            const post = table.find(post => post.id === id);

            // Якщо запис не знайдено, повернути null
            if (!post) {
                return null;
            }

            return post;
        } catch (err) {
            console.error(err);
            throw err;
        }
    }

    async delete(id) {
        try {
            let { schema, table } = await this.readTable();

            // Знайти індекс запису з вказаним ідентифікатором
            const index = table.findIndex(post => post.id === id);

            // Якщо запис не знайдено, викинути помилку
            if (index === -1) {
                throw new Error(`Record with id ${id} not found`);
            }

            // Видалити запис з таблиці
            const deletedId = table.splice(index, 1)[0].id;

            // Зберегти оновлені дані у файл
            await fsPromise.writeFile(this.filePath, JSON.stringify({ table, schema }));

            return deletedId;
        } catch (err) {
            console.error(err);
            throw err;
        }
    }


}



const fileDB = new FileDB();
fileDB.registerSchema('newspost', newspostSchema);

const newspostTable = fileDB.getTable('newspost');
const newspostTable2 = fileDB.getTable('newspost2');


const data = {
  title: 'У зоопарку Чернігова лисичка народила лисеня',
  text: "В Чернігівському зоопарку сталася чудова подія! Лисичка на ім'я Руда народила чудове лисенятко! Тож поспішайте навідатись та подивитись на це миле створіння!"
}


const createdNewspost = newspostTable.create(data);

const updateNewsposts = newspostTable.update(1, {title: "Маленька лисичка"});
const newsposts = newspostTable.getAll();
const newspost = newspostTable.getById(1);
const deletedId = newspostTable.delete(1);

